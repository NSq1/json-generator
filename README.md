# README #

### CONFIGURATION:

* server.port (default 8080)
* spring.profiles.active (random|static)

### API:
URL: {domain}/json/generate/{size}
> example: http://localhost:8081/json/generate/100


### Additionally 
if you wish to generate same data set every time there is a 'hidden' optional system property "seed" which defines a seed value that random generator should use for handling a request. 

Example JVM arguments:
>  -Dserver.port=8081 -Dspring.profiles.active=random -Dseed=2137


