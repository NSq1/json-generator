package com.nsq1.generator.json;

import com.nsq1.generator.json.service.ResponseGenerator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static org.mockito.Mockito.mock;

@Configuration
public class MockConfiguration {

    @Bean

    public ResponseGenerator responseGenerator() {
        return mock(ResponseGenerator.class);
    }
}
