package com.nsq1.generator.json.dto;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.util.MinimalPrettyPrinter;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;


public class ResponseEntityTest {
    private static final String EXPECTED_JSON = "{ " +
            "_type: \"Position\", " +
            "_id: 65483214, " +
            "key: null, " +
            "name: \"Oksywska\", " +
            "fullName: \"Oksywska, Poland\", " +
            "iata_airport_code: null, " +
            "type: \"location\", " +
            "country: \"Poland\", " +
            "geo_position: { latitude: 51.0855422, longitude: 16.9987442 }, " +
            "location_id: 756423, " +
            "inEurope: true, " +
            "countryCode: \"PL\", " +
            "coreCountry: true, " +
            "distance: null }";

    @Test
    public void shouldConvertToExpectedJson() throws Exception {
        ResponseEntity entity = new ResponseEntity()
                .set_type("Position")
                .set_id(65483214L)
                .setKey(null)
                .setName("Oksywska")
                .setFullName("Oksywska, Poland")
                .setIata_airport_code(null)
                .setType("location")
                .setCountry("Poland")
                .setGeo_position(new GeoPosition()
                        .setLatitude(51.0855422D)
                        .setLongitude(16.9987442D))
                .setLocation_id(756423L)
                .setInEurope(true)
                .setCountryCode("PL")
                .setCoreCountry(true)
                .setDistance(null);
        String actualJson = jsonWriter().writeValueAsString(entity);
        assertThat(actualJson, equalTo(EXPECTED_JSON));
    }

    private static ObjectWriter jsonWriter() {
        ObjectMapper mapper = new ObjectMapper();
        mapper = mapper.configure(JsonGenerator.Feature.QUOTE_FIELD_NAMES, false);
        return mapper.writer(new MinimalPrettyPrinter() {
            @Override
            public void writeStartObject(JsonGenerator g) throws IOException {
                super.writeStartObject(g);
                g.writeRaw(' ');
            }

            @Override
            public void writeEndObject(JsonGenerator g, int nrOfEntries) throws IOException {
                g.writeRaw(' ');
                super.writeEndObject(g, nrOfEntries);
            }

            @Override
            public void writeObjectFieldValueSeparator(JsonGenerator g) throws IOException {
                super.writeObjectFieldValueSeparator(g);
                g.writeRaw(' ');
            }

            @Override
            public void writeObjectEntrySeparator(JsonGenerator g) throws IOException {
                super.writeObjectEntrySeparator(g);
                g.writeRaw(' ');
            }
        });
    }
}