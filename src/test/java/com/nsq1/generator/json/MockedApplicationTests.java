package com.nsq1.generator.json;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;

@SpringBootTest
@Import(MockConfiguration.class)
class MockedApplicationTests {

    @Test
    void contextLoads() {
    }

}
