package com.nsq1.generator.json.dto;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class GeoPosition {
    private double latitude;
    private double longitude;
}
