package com.nsq1.generator.json.rest;

import com.nsq1.generator.json.dto.ResponseEntity;
import com.nsq1.generator.json.service.ResponseGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.logging.Logger;

@RestController
@RequestMapping("/json/generate/")
public class RestApi {
    @Autowired
    private ResponseGenerator responseGenerator;

    private final Logger log = Logger.getLogger(this.getClass().getName());

    @RequestMapping("/{size}")
    public List<ResponseEntity> generate(@PathVariable long size) {
        long start = System.currentTimeMillis();
        List<ResponseEntity> response = responseGenerator.generate(size);
        long end = System.currentTimeMillis();
        log.info(String.format("Response generation of size %s took %s ms", size, end - start));
        return response;
    }
}
