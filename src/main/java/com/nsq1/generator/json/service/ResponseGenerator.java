package com.nsq1.generator.json.service;

import com.nsq1.generator.json.dto.ResponseEntity;

import java.util.List;

public interface ResponseGenerator {
    List<ResponseEntity> generate(long quantity);
}
