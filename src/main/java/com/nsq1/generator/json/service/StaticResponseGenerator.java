package com.nsq1.generator.json.service;

import com.nsq1.generator.json.dto.GeoPosition;
import com.nsq1.generator.json.dto.ResponseEntity;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.LongStream;

@Component
@Profile("static")
public class StaticResponseGenerator implements ResponseGenerator {
    public static final ResponseEntity RESPONSE_ENTITY = new ResponseEntity()
            .set_type("Position")
            .set_id(65483214L)
            .setKey(null)
            .setName("Oksywska")
            .setFullName("Oksywska, Poland")
            .setIata_airport_code(null)
            .setType("location")
            .setCountry("Poland")
            .setGeo_position(new GeoPosition()
                    .setLatitude(51.0855422D)
                    .setLongitude(16.9987442D))
            .setLocation_id(756423L)
            .setInEurope(true)
            .setCountryCode("PL")
            .setCoreCountry(true)
            .setDistance(null);

    @Override
    public List<ResponseEntity> generate(long quantity) {
        return LongStream.range(0, quantity)
                .mapToObj(StaticResponseGenerator::staticResponseEntity)
                .collect(Collectors.toList());
    }

    private static ResponseEntity staticResponseEntity(long index) {
        return RESPONSE_ENTITY;
    }
}
