package com.nsq1.generator.json.service;

import com.nsq1.generator.json.dto.GeoPosition;
import com.nsq1.generator.json.dto.ResponseEntity;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.LongStream;

@Component
@Profile("random")
public class RandomResponseGenerator implements ResponseGenerator {
    private final static String ALPHABET = "abcdefghijklmnopqrstuvwxyz";

    @Override
    public List<ResponseEntity> generate(long quantity) {
        Random random = random();
        return LongStream.range(0, quantity).mapToObj(i -> generate(random)).collect(Collectors.toList());
    }

    private ResponseEntity generate(Random random) {
        String name = string(random, 40);
        String country = string(random, 24);
        return new ResponseEntity()
                .set_type(string(random, 20))
                .set_id(random.nextLong())
                .setKey(string(random, 64))
                .setName(name)
                .setFullName(name + ", " + country)
                .setIata_airport_code(string(random, 8))
                .setType(string(random, 12))
                .setCountry(country)
                .setGeo_position(geoposition(random))
                .setLocation_id(random.nextLong())
                .setInEurope(random.nextBoolean())
                .setCountryCode(string(random, 2))
                .setCoreCountry(random.nextBoolean())
                .setDistance(doubleInRange(random.nextDouble(), 9001D));
    }

    private double doubleInRange(double v, double l) {
        return v * l;
    }

    private String string(Random random, int length) {
        return IntStream.range(0, random.nextInt(length))
                .map(i -> random.nextInt(ALPHABET.length()))
                .mapToObj(ALPHABET::charAt)
                .map(String::valueOf)
                .collect(Collectors.joining());
    }

    private GeoPosition geoposition(Random random) {
        return new GeoPosition()
                .setLatitude(doubleInRange(doubleWithNegative(random), 90.0D))
                .setLongitude(doubleInRange(doubleWithNegative(random), 180.0D));
    }

    private double doubleWithNegative(Random random) {
        return 2.0D * random.nextDouble() - 0.5D;
    }

    private Random random() {
        return Optional.ofNullable(System.getProperty("seed")).map(Long::valueOf).map(Random::new).orElseGet(Random::new);
    }
}
